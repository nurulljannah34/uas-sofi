import React from 'react';
import{createStackNavigator} from '@react-navigation/stack';

import Splash from'../pages/splash';
import welcomeAuth from '../pages/welcome';
import andika from '../pages/andika';
import api from '../pages/api';
import bagimu from '../pages/bagimu';
import bangun from '../pages/bangun';
import bendera from '../pages/bendera';
import bunga from '../pages/bunga';
import merah from '../pages/merah';
import berkibar from '../pages/berkibar';
import benika from '../pages/benika';
import sabang from '../pages/sabang';

const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="welcome"
            component={welcomeAuth}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Andika Bhayangkara"
            component={andika}
            />
            <Stack.Screen
            name="Api Kemerdekaan"
            component={api}
            />
            <Stack.Screen
            name="Bagimu Negeri"
            component={bagimu}
            />
            <Stack.Screen
            name="Bangun Pemudi Pemuda"
            component={bangun}
            />
            <Stack.Screen
            name="Bendera Kita"
            component={bendera}
            />
            <Stack.Screen
            name="Bungaku"
            component={bunga}
            />
            <Stack.Screen
            name="Bendera Merah Putih"
            component={merah}
            />
            <Stack.Screen
            name="Berkibarlah Benderaku"
            component={berkibar}
            />
            <Stack.Screen
            name="Bhinneka Tunggal Ika"
            component={benika}
            />
            <Stack.Screen
            name="Dari Sabang Sampai Merauke"
            component={sabang}
            />
            </Stack.Navigator>
            
    );
};

export default Route;