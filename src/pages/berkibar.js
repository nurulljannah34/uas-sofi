import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class berkibar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Berkibarlah Benderaku</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>ciptaan: Ibu Soed</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Berkibarlah Benderaku</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah benderaku</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Lambang suci gagah perwira</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Di seluruh pantai Indonesia</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kau tetap pujaan bangsa</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Siapa berani menurunkan engkau</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Serentak rakyatmu membela</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Sang Merah Putih yang perwira</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah slama-lamanya</Text>, nomor:9},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kami rakyat Indonesia</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bersedia setiap masa</Text>, nomor:11},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Mencurahan segenap tenaga</Text>, nomor:12},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Supaya kau tetap cemerlang</Text>, nomor:13},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tak goyang jiwaku menahan rintangan</Text>, nomor:14},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Sang Merah Putih yang perwira</Text>, nomor:15},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah slama-lamanya.</Text>, nomor:16},



            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default berkibar;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});