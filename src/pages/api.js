import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class api extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Api Kemerdekaan</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Pencipta: Joko Lelono/Marlene</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Api Kemerdekaan</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Api Menyala di dalam dada satria</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Api Kemerdekaan bangsa</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Indonesia jaya</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Meski hancur lebur negara Kita</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Pahlawan Indonesia sedia</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkorban jiwa raga</Text>, nomor:9},
                

            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default api;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});