import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class welcomeAuth extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Andika Bhayangkara')}><Text style={styles.text}>Andika Bhayangkara</Text></TouchableOpacity>, nomor:1},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Api Kemerdekaan')}><Text style={styles.text}>Api Kemerdekaan</Text></TouchableOpacity>, nomor:2},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Bagimu Negeri')}><Text style={styles.text}>Bagimu Negeri</Text></TouchableOpacity>, nomor:3},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Bangun Pemudi Pemuda')}><Text style={styles.text}>Bangun Pemudi Pemuda</Text></TouchableOpacity>, nomor:4},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Bendera Kita')}><Text style={styles.text}>Bendera Kita</Text></TouchableOpacity>, nomor:5},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Bungaku')}><Text style={styles.text}>Bungaku</Text></TouchableOpacity>, nomor:6},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Bendera Merah Putih')}><Text style={styles.text}>Bendera Merah Putih</Text></TouchableOpacity>, nomor:7},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Berkibarlah Benderaku')}><Text style={styles.text}>Berkibarlah Benderaku</Text></TouchableOpacity>, nomor:8},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Bhinneka Tunggal Ika')}><Text style={styles.text}>Bhennika Tunggal Ika</Text></TouchableOpacity>, nomor:9},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Dari Sabang Sampai Merauke')}><Text style={styles.text}>Dari Sabang Sampai Merauke</Text></TouchableOpacity>, nomor:10},
            ],

        };
    }
    render () {
    return (
        <View style={{flex:1}}>
                <View style={{alignItems:'center',justifyContent:'center',flex:1,backgroundColor:'black'}}>
                        <Text style={{fontSize:30,marginTop:5,textAlign:'center',color:'white',fontFamily:'times new roman'}}>List Lagu Nasional</Text>                
                    </View>
                <View style={{flex:8}}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                        numColumns={numColumns}
                    />
                </View>
            </View>
    );
    }
};

export default welcomeAuth;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    gambar : {
        height: WIDTH / (numColumns * 1),
        margin:5,
        padding:5,
        backgroundColor:'dimgray',
        alignItems:'center',
        justifyContent:'center',
        width:300,
        height:70,
        marginHorizontal:30,
        borderRadius:15,
    },
    text: {
        color: 'white',
        fontFamily:'times new roman',
        fontSize: 16
    }
});