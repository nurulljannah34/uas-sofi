import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';


const Splash = ({navigation}) => {
    useEffect(( ) => {
        setTimeout(( ) => {
            navigation.replace ('welcome');
        }, 5000);
    });
    return(
        <View style={styles.wrapper}>
         <Text style= {styles.welcomeText}>Lagu Nasional</Text>
         <Image source={require('../assets/foto.jpg')} style={{width :200, height: 300}}/>
        </View> 
        );
    };

export default Splash;
const styles = StyleSheet.create({
   wrapper: {
       flex: 1,
       backgroundColor: 'black',
       alignItems: 'center',
       justifyContent: 'center',
   },
  
   welcomeText: {
       fontSize:30,
       fontWeight: 'bold',
       color: 'white',
       paddingBottom: 20,
   },
});
    