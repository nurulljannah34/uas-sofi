import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class sabang extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Dari sabang sampai Merauke</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Ciptaan: R. Soerardjo</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Dari sabang sampai Merauke</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Dari sabang sampai merauke</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berjajar pulau-pulau</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Sambung memnyambung menjadi satu</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Itulah Indonesia</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Indonesia tanah airku</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Aku berjanji padamu</Text>, nomor:9},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Menjunjung tanah airku</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tanah airku Indonesia</Text>, nomor:11},

            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default sabang;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});