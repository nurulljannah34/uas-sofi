import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class bagimu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Bagimu Negeri</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Pencipta: R kusbini</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Bagimu Negeri</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Padamu Negeri kami berjanji</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Padamu Negeri kami berbakti</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Padamu Negeri kami mengabdi</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bagimu Negeri jiwa raga kami</Text>, nomor:7},




            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default bagimu;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});