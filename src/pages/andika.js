import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class andika extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Andika Bhayangkari</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Pencipta: Amir Pasaribu</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Andika Bhayangkari</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Pencipta sapta marga</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Pancasila mulai jadi negara mulia</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Bhennika tunggal ika</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Lambang bangsa satria</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Menuju nusantara</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Bahagia jaya</Text>, nomor:9},
                {gambar:<Text style={{marginLeft:10,marginLeft:10,color:'white',fontSize:17}}>Bahagia jaya</Text>, nomor:10},
                


            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default andika;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});