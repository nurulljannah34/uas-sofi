import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class benika extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Bhennika tunggal Ika.</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Pencipta: Binsar sitompul/A. Thallib</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik lagu Bhennika tunggal Ika</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah benderaku</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah benderaku</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah benderaku</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah benderaku</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Lambang suci gagah perwira</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Di seluruh pantai Indonesia</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kau tetap pujaan bangsa</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Siapa berani menurunkan engkau</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Sang Merah Putih yang perwira</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Sang Merah Putih yang perwira</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah slama-lamanya</Text>, nomor:11},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kami rakyat Indonesia</Text>, nomor:12},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bersedia setiap masa</Text>, nomor:13},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Mencurahan segenap tenaga</Text>, nomor:14},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Supaya kau tetap cemerlang</Text>, nomor:15},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tak goyang jiwaku menahan rintangan</Text>, nomor:16},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tak gentar rakyatmu berkorban</Text>, nomor:17},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berkibarlah slama-lamanya.</Text>, nomor:18},

            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default benika;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});