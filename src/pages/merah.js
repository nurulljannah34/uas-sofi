import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class merah extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Bendera Merah Putih</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Ciptaan: Ibu Soed</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Bendera Merah Putih</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berdera merah putih</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bendera tanah airku</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Gagah dan jernih tampak warnamu</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bendera merah putih</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bendera bangsaku</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berdera merah putih</Text>, nomor:9},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Pelambang brani dan suci</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Siap selalu kami</Text>, nomor:11},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Untuk bangsa dan ibu pertiwi</Text>, nomor:12},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Berdera merah putih</Text>, nomor:13},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Trimalah salamku</Text>, nomor:14},


            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default merah;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});