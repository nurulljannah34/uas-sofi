import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class bendera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Bendera Kita</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Pencipta: Dirman sasmokoadi</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Bendera Kita</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}> Rasakan kau Rasakan kau di atas bahu</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tanggungjawab dari yang satu</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Disini aku berlabuh</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Biar sampai tumbuhku hancur</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tertulis barisan cerita</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Putera puteri kita si pejuang bangsa</Text>, nomor:9},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kita ketepikan soal warna</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Walau beza kau tetap sama</Text>, nomor:11},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kibarkan, ah-a-a</Text>, nomor:12},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bendera kita</Text>, nomor:13},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}} >Kayanya tanah ibuku</Text>, nomor:14},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Melimpah nikmat buat rakyatmu</Text>, nomor:15},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Apakah pendirianmu</Text>, nomor:16},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Jangan tergadai sampai hilangnya</Text>, nomor:17},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tanah tumpah darahku</Text>, nomor:18},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tertulis barisan cerita</Text>, nomor:19},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Putera puteri kita si pejuang bangsa</Text>, nomor:20},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kita ketepikan soal warna</Text>, nomor:21},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Walau beza kau tetap sama</Text>, nomor:22},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Kibarkan, ah-a-a</Text>, nomor:23},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bendera kita</Text>, nomor:24},

            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default bendera;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});