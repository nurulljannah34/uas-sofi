import React, { Component } from 'react';
import {Image, StyleSheet, Text, View,FlatList, Dimensions} from 'react-native';

class bangun extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar:<Text style={{color:'white',fontSize:17}}>Bangun Pemudi Pemuda</Text>, nomor:1},
                {gambar:<Text style={{color:'white',fontSize:17}}>Pencipta: Alfred Simanjuntak</Text>, nomor:2},
                {gambar:<Text style={{marginTop:10,marginLeft:10,color:'white',fontSize:17}}>Lirik Lagu Bangun Pemudi Pemuda</Text>, nomor:3},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>kami putra dan putri Indonesia</Text>, nomor:4},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Mengaku berbangsa satu, bangsa Indonesia</Text>, nomor:5},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>kami putra dan putri Indonesia</Text>, nomor:6},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Mengaku bertanah air satu, tanah air Indonesia</Text>, nomor:7},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>kami putra dan putri Indonesia</Text>, nomor:8},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Menjunjung tinggi bahasa persatuan, bahasa Indonesia</Text>, nomor:9},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bangun pemudi pemuda Indonesia</Text>, nomor:10},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tangan bajumu singsingkan untuk negara</Text>, nomor:11},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Masa yang akan datang kewajibanmu lah</Text>, nomor:12},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Menjadi tanggunganmu terhadap nusa</Text>, nomor:13},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Sudi tetap berusaha jujur dan kuat</Text>, nomor:14},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tak usah banyak bicara trus kerja keras</Text>, nomor:15},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Hati teguh dan lurus pikir tetap jernih</Text>, nomor:16},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bertingkah laku halus hai putra negri</Text>, nomor:17},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bertingkah laku halus hai putra negri</Text>, nomor:18},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Bangun pemudi pemuda Indonesia</Text>, nomor:19},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Tangan bajumu singsingkan untuk negara</Text>, nomor:20},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Masa yang akan datang kewajibanmu lah</Text>, nomor:21},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Menjadi tanggunganmu terhadap nusa</Text>, nomor:22},
                {gambar:<Text style={{marginLeft:10,color:'white',fontSize:17}}>Menjadi tanggunganmu terhadap nusa.</Text>, nomor:23},
            ],

        };
    }
    render () {
    return (
        <View style={{flex:1,backgroundColor:'black'}}>
             <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                            />
        </View>
    );
    }
};

export default bangun;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    
});